Source: python-sphinx-codeautolink
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael R. Crusoe <crusoe@debian.org>
Build-Depends: debhelper-compat (= 13),
               pybuild-plugin-pyproject,
               python3-setuptools,
               python3-all,
               python3-sphinx,
               python3-bs4,
               python3-pytest <!nocheck>,
               python3-ipython <!nocheck>,
               python3-pickleshare <!nocheck>,
               python3-doc <!nocheck> <!nodoc>,
               python-numpy-doc <!nodoc>,
               python-matplotlib-doc <!nodoc>
Standards-Version: 4.7.0
Homepage: https://sphinx-codeautolink.rtfd.io/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-sphinx-codeautolink
Vcs-Git: https://salsa.debian.org/python-team/packages/python-sphinx-codeautolink.git
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no

Package: python3-sphinx-codeautolink
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
Enhances: python3-sphinx
Description: Automatic links from code examples to reference documentation
 This plugin for the Sphinx documentation tool makes code examples clickable by
 inserting links from individual code elements to the corresponding reference
 documentation. The aim is for a minimal setup assuming your examples are
 already valid Python.
 .
 For a live demo, see the online documentation at
 https://sphinx-codeautolink.rtfd.org
